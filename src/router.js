import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, routerRedux, Switch } from 'dva/router';
import dynamic from 'dva/dynamic';
import App from 'routes/app';

const { ConnectedRouter } = routerRedux

const Routers = function({ history, app }) {
  const error = dynamic({
    app,
    component: () => import('./routes/error'),
  })
  const routes = [{
    path: '/login',
    models: () => [import('./models/login')],
    component: () => import('./routes/login/'),
  }, {
    path: '/wallet',
    models: () => [import('./models/wallet')],
    component: () => import('./routes/wallet/'),
  }, {
    path: '/dashboard',
    models: () => [import('./models/dashboard')],
    component: () => import('./routes/dashboard/'),
  }, {
    path: '/users',
    models: () => [import('./models/users')],
    component: () => import('./routes/users/'),
  }, {
    path: '/user/:id',
    models: () => [import('./models/user')],
    component: () => import('./routes/user/'),
  },
  {
    path: '/collectionAccountManagement',
    models: () => [import ('./models/collectionAccountManagement')],
    component: () => import ('./routes/collectionAccountManagement/')
  },
  {
    path: '/addCount',
    models : () => [import ('./models/addCount')],
    component: () => import ('./routes/addCount')
  },
  {
      path: '/withdraw',
      models: () => [import('./models/withdraw')],
      component: () => import('./routes/withdraw/'),
  },
  ]

  return (
    <ConnectedRouter history={history}>
      <App>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/wallet"/>)}/>
          {
            routes.map(({ path, ...dynamics }, key) => (
              <Route key={key} exact path={path} component={dynamic({ app, ...dynamics })} />
            ))
          }
          <Route component={error}/>
        </Switch>
      </App>
    </ConnectedRouter>
  )
}

Routers.propTypes = {
  history: PropTypes.object,
  app: PropTypes.object,
}

export default Routers
