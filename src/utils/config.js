const APIV1 = '/transfer-express-services/api'

module.exports = {
  name: '亚马逊收款管理',
  prefix: 'easypayxAdmin',
  footerText: 'EasyPay Design Admin  © 2017',
  logo: '/logo.png',
  addIcon: '/assets/addIcon.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  CORS: [],
  openPages: ['/login'],
  apiPrefix: '/api',
  APIV1,
  api: {
    userLogin: `/transfer-express-services/login`,
    userLogout: `${APIV1}/user/logout`,
    userInfo1: `${APIV1}/userInfo`,
    users: `${APIV1}/admin/users`,
    user: `${APIV1}/users/:id/view`,
    userLegalEntities: `${APIV1}/admin/users/:id/entities`,
    userInfo: `${APIV1}/users/:id/view`,
    userTransactions: `${APIV1}/users/:id/transactions`,
    userDetail: `${APIV1}/aiyou/users/:id/detail`,
    userAuths: `${APIV1}/admin/users/validating`,
    v1test: `${APIV1}/test`,
    updateUserFeeRate: `${APIV1}/admin/user-fee-rates`,
    userBanks: `${APIV1}/users/:id/banks?currency=1`,
    withdrawsFee: `${APIV1}/users/:id/withdraws/fee`,
    applyWithdraws: `${APIV1}/udas/:id/withdraws`,
    // 收款账户管理相关url
    submitAuthticationUrl : `${APIV1}/transfer-express-services/api/aiyou/users/:id/authentication`,
    accoutUsersUrl: `${APIV1}/users/:id/accoutUsers`,
    deleteAccoutUrl: `${APIV1}/users/:id/deleteUsers`
  },
}
