/* global window */
import classnames from 'classnames'
import lodash from 'lodash'
import config from './config'
import request from './request'
import {color} from './theme'
import axios from 'axios'
import qs from 'qs'

// 连字符转驼峰
String.prototype.hyphenToHump = function () {
  return this.replace(/-(\w)/g, (...args) => {
    return args[1].toUpperCase()
  })
}

// 驼峰转连字符
String.prototype.humpToHyphen = function () {
  return this
    .replace(/([A-Z])/g, '-$1')
    .toLowerCase()
}

// 日期格式化
Date.prototype.format = function (format) {
  const o = {
    'M+': this.getMonth() + 1,
    'd+': this.getDate(),
    'h+': this.getHours(),
    'H+': this.getHours(),
    'm+': this.getMinutes(),
    's+': this.getSeconds(),
    'q+': Math.floor((this.getMonth() + 3) / 3),
    S: this.getMilliseconds()
  }
  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, `${this.getFullYear()}`.substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp(`(${k})`).test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length === 1
        ? o[k]
        : (`00${o[k]}`).substr(`${o[k]}`.length))
    }
  }
  return format
}

/**
 * @param   {String}
 * @return  {String}
 */

const queryURL = (name) => {
  let reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i')
  let r = window
    .location
    .search
    .substr(1)
    .match(reg)
  if (r != null) 
    return decodeURI(r[2])
  return null
}

/**
 * 数组内查询
 * @param   {array}      array
 * @param   {String}    id
 * @param   {String}    keyAlias
 * @return  {Array}
 */
const queryArray = (array, key, keyAlias = 'key') => {
  if (!(array instanceof Array)) {
    return null
  }
  const item = array.filter(_ => _[keyAlias] === key)
  if (item.length) {
    return item[0]
  }
  return null
}

/**
 * 数组格式转树状结构
 * @param   {array}     array
 * @param   {String}    id
 * @param   {String}    pid
 * @param   {String}    children
 * @return  {Array}
 */
const arrayToTree = (array, id = 'id', pid = 'pid', children = 'children') => {
  let data = lodash.cloneDeep(array)
  let result = []
  let hash = {}
  data.forEach((item, index) => {
    hash[data[index][id]] = data[index]
  })

  data.forEach((item) => {
    let hashVP = hash[item[pid]]
    if (hashVP) {
      !hashVP[children] && (hashVP[children] = [])
      hashVP[children].push(item)
    } else {
      result.push(item)
    }
  })
  return result
}


/**
* 封装ajax-- post 请求,请求自带header属性以及Authorization 字段
 * @param {String} url  请求地址
 * @param {Object} params  请求参数
 */
export const ajaxPost = (url, params) => {
  let axiosInstance = axios.create({
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': sessionStorage.getItem("auth")
    },
    // timeout: 5000 这里可以设置请求超时时间，nginx服务器一般默认10s(10000) 这里相当于拦截请求，对参数做一次处理
    transformRequest: [function (data) {
        var data = data || {};
        // Do whatever you want to transform the data
        var auth = sessionStorage.getItem("auth")
        if (auth == null) {
          window.location.href = window.location.origin + window.location.pathname + '#/login';
        } else {
          data.Authorization = auth;
        }
        return JSON.stringify(data);
      }
    ],
    transformResponse: [function (data) {
        var data = typeof data == 'string'
          ? JSON.parse(data)
          : data;
        if (data != null) {
          // 异常处理 if (data.code ！== ) {   window.location.href = window.location.origin +
          // window.location.pathname + '#/login'; }
        }
       
      }
    ]
  });
  return axiosInstance.post(url, params).then((response) => {
    return Promise.resolve({
          success: true,
          message: statusText,
          statusCode: status,
          headers: response.headers,
          data: { ...data },
          // ...data,
        })
  })
}

/**
 * 封装ajax --get 请求
 * @param {Strig} url
 * @param {Object} params
 * @param {Boolean} isQuery 是否需要添加’？‘，true:是，false:否
 */
export const ajaxGet = (url, params, isQuery) => {
  let axiosInstance = axios.create({
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': sessionStorage.getItem("auth")
    }
  });
  var data = {};
  const auth = sessionStorage.getItem("auth")
  if (auth == null) {
    // 如果没有登录信息，回退到登录页面
    window.location.href = window.location.origin + window.location.pathname + '#/login';
  } else {
    //授权也以参数形式发送一份
    data.Authorization = auth;
  }
  data = lodash.cloneDeep(data, params
    ? params
    : {});
  var queryParams = qs.stringify(data, {
    addQueryPrefix: !!isQuery
      ? isQuery
      : true
  });

  return axiosInstance.get(url, {params: data}).then((response)=>{
    console.log(response);
    let { status, statusText, data} = response;
    if(status == 200){

      return Promise.resolve({
        success: true,
        message: statusText,
        statusCode: status,
        headers: response.headers,
        data: { ...data },
        // ...data,
      })
    } else {
      return Promise.reject({
          success : false,
          status,
          message : statusText
      })
    }
  })
}

module.exports = {
  config,
  request,
  color,
  classnames,
  queryURL,
  queryArray,
  arrayToTree,
  ajaxPost,
  ajaxGet
}
