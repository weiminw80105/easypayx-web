import { request, config } from 'utils'
import QuryString from 'query-string'
const { api } = config
const { userAuths } = api
/**
 * 获取实名认证列表
 * @returns {Promise.<*>}
 */
export async function fetchUserAuths () {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userAuths,
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
  })
}




