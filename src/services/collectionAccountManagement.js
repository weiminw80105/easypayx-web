import {request, config, ajaxPost, ajaxGet} from 'utils'

const {api} = config
const {
    submitAuthticationUrl,
    accoutUsersUrl,
    deleteAccoutUrl,
    userTransactions
   } = api

/**
 * 
 * @param {*} data 
 */
export async function submitAuthtication(data) {
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    return ajaxPost(submitAuthticationUrl)
}

/**
 * 获取token
 */
export async function fetchToken() {
    return ajaxGet(`/transfer-express-services/api/qiniu/upload/token`)
}

/**
 * 获取账户
 * @param {*} params 
 */
export async function getAccoutUsers(params) {
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    return ajaxGet(userTransactions.replace(':id', 164))
}

/**
 * 删除账户
 * @param {*} params 
 * params中应该包含id
 */
export async function deleteAccoutUsers(params) {
    console.log(params);
   return ajaxGet(deleteAccoutUrl,{userInd: params.userId})
}

