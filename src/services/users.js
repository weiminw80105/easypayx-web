import { request, config } from 'utils'
import QuryString from 'query-string'
const { api } = config
const { userInfo,userTransactions } = api

export async function fetchGatewayUserInfo () {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userInfo.replace(':id', 164),
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
  })
}

export async function fetchUserTransactions () {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userTransactions.replace(':id', 164),
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
  })
}





