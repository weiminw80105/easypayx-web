import { request, config } from 'utils'

const { api } = config
const { updateUserFeeRate, userDetail, userLegalEntities } = api

export async function query (params) {
  console.log(params);
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: userDetail.replace(':id',params.id),
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}

export async function queryUserLegalEntities ({userId}) {
  const auth = sessionStorage.getItem("auth")
  console.log(auth);

  return request({
    url: userLegalEntities.replace(':id', userId),
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}


