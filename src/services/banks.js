import {request, config} from 'utils'
const {api} = config
const {userBanks, withdrawsFee, applyWithdraws} = api

export async function fetchUserBanks() {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userBanks.replace(':id', 164),
    method: 'get',
    headers: {
      Authorization: auth,
      'Content-Type': 'application/json'
    },
    data: {}
  })

}

export function caculateWithdrawFee({userId, fromAmount, fromCurrency, toCurrency}) {
  const auth = sessionStorage.getItem("auth");

  return request({
    url: withdrawsFee.replace(':id', 164),
    method: 'POST',
    headers: {
      Authorization: auth,
      'Content-Type': 'application/json'
    },
    data: {
      fromAmount,
      fromCurrency,
      toCurrency
    }
  })
}

export function submitWithdraw({
  userId,
  userDigitalAccountId,
  bankAccount,
  amount,
  currency,
  transPasswd
}) {
  const auth = sessionStorage.getItem("auth");
  const error = request({
    url: applyWithdraws.replace(':id', userDigitalAccountId),
    method: 'POST',
    headers: {
      Authorization: auth
    },
    data: {
      userId,
      userDigitalAccountId,
      bankAccount,
      amount,
      currency,
      transPasswd
    }
  })
  return error;
}
