import React from 'react';
import { Button, Form, Input, InputNumber, Select, Alert, Row, Card, Radio } from 'antd';
import * as transactionService from '../../../services/banks';


import { Link } from 'dva/router'
import styles from '../index.less'

const FormItem = Form.Item;
const Option = Select.Option

const currencyName = ['', '美元', '人民币', '欧元', '日元'];

class WithdrawEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      withdrawFeeResponse: {},
      hasSubmited: this.props.initData.error || false,
      perpareCommit: false,
      mode: 'china',
      currency: '2',
    };
  }

  onSubmit() {
    this.props.form.validateFieldsAndScroll(["transPasswd"],(err, values) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        this.props.onSubmit(formData);
        this.setState({
          hasSubmited:true,
        });

      };
    });
  }

  onPerpareCommit() {
    this.props.form.validateFieldsAndScroll(["bankAccount","shopAccount","currency","amount"],(err, values) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        this.caculateWithdrawFee();
        this.setState({
          perpareCommit:true,
        });
      };
    });
  }

  checkWithdrawAmount(rule, value, callback) {
    if (value) {
      const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
      if (parseFloat(value) > parseFloat(userInfo.balance)) {
        callback('提现金额不能超过当前账户余额');
      } else if (parseFloat(value) <= 0) {
        callback('请输入正确的提现金额');
      } else if (parseFloat(value) <= 2) {
        callback('最低提现金额为不低于2美金');
      } else {
        callback();
      }
    } else {
      callback('请输入正确的提现金额');
    }
  }


  caculateWithdrawFee(type, value) {
    const formData = this.props.form.getFieldsValue();
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));

    let fromCurrencyCode = formData.currency
    if (type == "from")
      fromCurrencyCode = value

    let toCurrencyCode = formData.shopAccount
    if (type == "to")
      toCurrencyCode = value

    transactionService.caculateWithdrawFee(
      { userId:userInfo.id, fromAmount: formData.amount, fromCurrency: fromCurrencyCode, toCurrency: toCurrencyCode },
    ).then(({ data }) => {
      this.setState({
        withdrawFeeResponse: data,
      });
    });
  }

  handleModeChange(e) {

    const value = e.target.value;
    this.setState({
      withdrawFeeResponse: {},
      perpareCommit: false,
      mode: value,
    });
    this.props.form.setFieldsValue({
      currency: "2",
      shopAccount: "",
      bankAccount: "",
      amount: "",
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 8 },
    };
    const userBanksSelect = [];
    this.props.userBanks.map(function(item) {
      userBanksSelect.push(
        <Option value={'' + item.id} key={item.id}>
          {item.bankName} - {item.accountHolderName} ({item.accountNumber})
        </Option>);
    })


    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    const formData = this.props.form.getFieldsValue();

    let bankContent ="";

    this.props.userBanks.map(function(item) {
      if (item.id == formData.bankAccount)
      {
        bankContent = item.bankName +"尾号[" + item.accountNumber.substr(item.accountNumber.length-4,4) + "]";
      }
    })


    return (

    <div className={styles.form}>

      <div className={styles.pannelTitle} style={{display: this.state.perpareCommit ? 'none' : '' }} >
        <div>
          <p>提现申请</p>
        </div>


        <Radio.Group onChange={this.handleModeChange.bind(this)} value={this.state.mode} className={styles.link1}>
          <Radio.Button value="china">中国站</Radio.Button>
          <Radio.Button value="international">国际站</Radio.Button>
        </Radio.Group>

      </div>

      <div className={styles.pannelTitle} style={{display: this.state.perpareCommit ? '' : 'none' }} >
        <div>
          <p>提现确认</p>
        </div>
      </div>


      <div className={styles.accountForm}>
        <div className={styles.withdrawSite}>

      <Form style={{marginTop:"32px"}} layout="horizontal" className={styles.tableWrap} >

        <div style={{display: this.state.perpareCommit ? 'none' : '' }} >

        <FormItem
          {...formItemLayout}
          label='提现银行卡'
          extra='添加银行卡'

        >
          {
            getFieldDecorator('bankAccount', {
              rules: [
                { required: true, message: '提现银行卡不能为空' }
              ]
            })(<Select>
              {userBanksSelect}

            </Select>)
          }
        </FormItem>

        <FormItem
          {...formItemLayout}
          label='提现账户'

        >
          {
            getFieldDecorator('shopAccount', {
              rules: [
                { required: true, message: '提现账户不能为空' }
              ]
            })(<Select onChange={this.caculateWithdrawFee.bind(this,"to")}>

              <Option value='1' key='1'>
                美国银行{this.state.shopAccount}
              </Option>
              <Option value='3' key='3'>
                欧洲银行
              </Option>

            </Select>)
          }
        </FormItem>


        <FormItem
          {...formItemLayout}
          label='提现币种'
        >
        {
          getFieldDecorator('currency', {
            initialValue: this.state.currency,
            rules: [
              { required: true, message: '提现币种不能为空' }
            ]
          })(<Select disabled={this.state.mode == "china"?true:false} onChange={this.caculateWithdrawFee.bind(this,"from")}>
            <Option value="2">{currencyName[2]}</Option>
            <Option value="1">{currencyName[1]}</Option>
            <Option value="3">{currencyName[3]}</Option>
            <Option value="4">{currencyName[4]}</Option>
          </Select>)
        }
        </FormItem>


        <FormItem
          {...formItemLayout}
          label="当前汇率"
        >
          <span>{this.state.withdrawFeeResponse.rate}</span>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="提现金额"
          required="rue"
          extra="最低提现金额为10美金"

        >
          {
            getFieldDecorator('amount', {
              initialValue: null,
              rules: [
                { type: 'number' },
                { validator: this.checkWithdrawAmount.bind(this) }
              ],
            })(<InputNumber min={10} max={2000000}/>)
          }
        </FormItem>


          <Row className={styles.commonRow}>
            <Button type="primary" size="large" className={styles.btnBgColor} onClick={this.onPerpareCommit.bind(this)} disabled={this.props.initData.error ? false : this.state.hasSubmited}>确认</Button>
            <span className={styles.returnAnchor} >返回</span>
          </Row>

        </div>



        <div style={{display: this.state.perpareCommit ? '' : 'none' }} >

          <FormItem
            {...formItemLayout}
            label="实际入账"
          >
            <span>{this.state.withdrawFeeResponse.toAmount}</span>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="提现银行卡"
          >
            <span>{bankContent}</span>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="币种"
          >
            <span>{currencyName[formData.currency]}</span>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="汇率"
          >
            <span>{this.state.withdrawFeeResponse.rate}</span>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="手续费"
          >
            <span>{this.state.withdrawFeeResponse.fee}{currencyName[this.state.withdrawFeeResponse.toCurrency]}</span>
          </FormItem>




          <Card>

          <FormItem
            {...formItemLayout}
            label='支付密码'
            extra="请您输入由8位以上数字、字母、符号所组成的支付密码"
          >
            {
              getFieldDecorator('transPasswd', {
                initialValue: '',
                rules: [
                  { required: true, message: '请填写支付密码' },
                ],
              })(<Input type="password" />)
            }
          </FormItem>


          <FormItem
            {...formItemLayout}
            label="到账时间"
            extra={ this.props.initData.error?<Alert
              showIcon="true"
              message={this.props.initData.error}
              type="error"
            />:""}
          >
            <span>24小时到账,节假日将顺延</span>
          </FormItem>





          <Row className={styles.commonRow}>
            <Button type="primary" size="large" className={styles.btnBgColor} onClick={this.onSubmit.bind(this)} disabled={this.props.initData.error ? false : this.state.hasSubmited}>确认</Button>
            <span className={styles.returnAnchor} >返回</span>
          </Row>

          </Card>


        </div>
      </Form>
      </div>
      </div>
    </div>
    );
  }
}



export default Form.create()(WithdrawEdit);
