import React, { PropTypes } from 'react'
import styles from '../index.less'
import { connect } from 'dva';
import { Link } from 'dva/router';

import { Col, Row, Steps,Button } from 'antd';

const Step = Steps.Step;

function WithdrawCompleted ({ withdrawData }) {
  console.log("withdrawData111111");
  console.log(withdrawData);
  return (

    <div className={styles.normal}>

      <Row type="flex" justify="space-around" align="middle">
        <Col span={16} className={'text-center ' + styles.section}></Col>

      </Row>
      <Row type="flex" justify="center" align="middle">
        <Col span={16} className={'text-center ' + styles.section}><h3>提现申请提交成功,系统处理中</h3></Col>

      </Row>
      <Row type="flex" justify="center" align="middle">
        <Col span={16} className={'text-center ' + styles.section}>您可以在<Link to={"/wallet"}><span
          className="h5">'交易记录'</span></Link>中查看处理进度</Col>

      </Row>
      <Row type="flex" justify="center" align="middle">
        <Col span={16} className={styles.section}>
          <Steps>
            <Step title="申请提现" status="finished" description={withdrawData.createTime}></Step>
            <Step title="系统处理" status="process" description={withdrawData.createTime}></Step>
            <Step title="预计到账" status="wait" description="24小时后,节假日将顺延"></Step>
          </Steps>
        </Col>
      </Row>
    </div>
  )
}

WithdrawCompleted.propTypes = {
  withdrawData: PropTypes.object
}

export default WithdrawCompleted
