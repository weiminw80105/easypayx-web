import React from 'react'
import PropTypes from 'prop-types'
import md5 from 'md5';
import { connect } from 'dva'
import { Button, Row, Col, Table, Card, Tabs, Form, Radio,Steps,message } from 'antd'

import { config } from 'utils'
import styles from './index.less'
import {WithdrawEdit,WithdrawCompleted} from './components/'


class Withdraw extends React.Component {
  state = {
    successed: false,
    errorMessage: '',
  }

  submitWithdrawRequest(formData) {
    const userInfo = this.props.user;
    const { dispatch } = this.props;
    const _this = this;

    dispatch({
      type: 'withdraw/submitWithdrawRequest',
      payload: {
        userId: userInfo.id,
        userDigitalAccountId: userInfo.digitalAccountId,
        bankAccount: formData.bankAccount,
        amount: formData.amount,
        currency: 1,
        transPasswd: md5(formData.transPasswd),
      },
      onError(error) {
        _this.setState({
          errorMessage: error.message,
        });
      },
    });
  }
  render() {
    let content = <WithdrawEdit
      initData={{ error: this.state.errorMessage }}
      userBanks={this.props.userBanks ? this.props.userBanks : []}
      onSubmit={this.submitWithdrawRequest.bind(this)}
    />
    if (this.props.withdrawTransaction) {
      content = <WithdrawCompleted withdrawData={this.props.withdrawTransaction ? this.props.withdrawTransaction : {}}/>;
    }
    return (
      <div>
      {content}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { userBanks, withdrawTransaction } = state.withdraw;
  const {user} = state.app;
  return {
    loading: state.loading.models.withdraw,
    userBanks,
    withdrawTransaction,
    user,
  };
}
export default connect(mapStateToProps)(Withdraw);
