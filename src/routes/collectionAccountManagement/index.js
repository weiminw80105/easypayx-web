/*
 * @Author: Elaine.panmin
 * @Date: 2017-10-14 22:37:03
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2017-10-19 22:16:57
*/
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'dva'
import {
  Button,
  Row,
  Form,
  Input,
  Table,
  Icon,
  Popover,
  Modal
} from 'antd'
import {config} from 'utils'
import {routerRedux} from 'dva/router'
import styles from './index.less'

const FormItem = Form.Item

const statusMap = {
  0: '成功',
  1: '失败',
  2: '审核中'
};

class CollectionAccountManagement extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '美国银行账号',
        dataIndex: 'accountId',
        key: 'accountId',
        width: 150,
        className: 'accountId'
      }, {
        title: '店铺名',
        dataIndex: 'storeName',
        key: 'storeName'
      }, {
        title: '路由号（9位）',
        dataIndex: 'routeNumber',
        key: 'routeNumber'
      }, {
        title: '状态',
        dataIndex: 'accountStatus',
        key: 'accountStatus',
        render: (text, record) => {
          const content = (
            <div className={styles.tipsWraper}>
              <p
                style={{
                display: record.accountStatus == 1
                  ? 'block'
                  : 'none'
              }}>
                <a onClick={this.handleDeleteAccount.bind(this,record)}>删除</a>
              </p>
              <p
                style={{
                display: record.accountStatus == 1
                  ? 'block'
                  : 'none'
              }}>
                <a onClick={this.handleModifyAccount.bind(this,record)}>修改</a>
              </p>
              <p
                style={{
                display: record.accountStatus == 0
                  ? 'block'
                  : 'none'
              }}>
                <a onClick={this.handleCloseAccount.bind(this,record)}>关闭</a>
              </p>
            </div>
          );
          return (
            <div>
              <a
                className={record.accountStatus == 0
                ? styles.statusSuccess
                : (record.accountStatus == 1
                  ? styles.statusFailure
                  : styles.statusChecking)}
                href="#">
                {statusMap[record.accountStatus]}
              </a>
              {record.accountStatus != 2
                ? <Popover
                    placement="bottomLeft"
                    content={content}
                    arrowPointAtCenter
                    trigger='click'>
                    <span className={styles.tipsIcon}><Icon type="ellipsis"/></span>
                  </Popover>
                : <span className={styles.tipsIcon}><Icon
                  type="ellipsis"
                  style={{
                  color: 'grey',
                  cursor: 'not-allowed'
                }}/></span>}

            </div>
          )
        }
      }
    ];

    this.state = {
      dataSource: [
        {
          accountId: '2017-09-10 11:11:01',
          storeName: '1245.121',
          accountStatus: '0', //0 成功，1失败 ，2 审核中
          routeNumber: 'fas'
        }, {
          accountId: '2017-09-10 11:11:02',
          storeName: '1245.122',
          accountStatus: '1',
          routeNumber: 'fas'
        }, {
          accountId: '2017-093-10 11:11:03',
          storeName: '1245.123',
          accountStatus: '2',
          routeNumber: 'fas'
        }, {
          accountId: '2017-09-10 11:11:04',
          storeName: '1245.124',
          accountStatus: '0',
          routeNumber: 'fas'
        }, {
          accountId: '2017-09-10 11:11:05',
          storeName: '1245.125',
          accountStatus: '1',
          routeNumber: 'fas'
        }, {
          accountId: '2017-09-10 11:11:06',
          storeName: '1245.126',
          accountStatus: '2',
          routeNumber: 'fas'
        }, {
          accountId: '2017-09-10 11:11:07',
          storeName: '1245.127',
          accountStatus: '2',
          routeNumber: 'fas'
        }
      ],
      visible: false, //模态框是否可见
      loading: false, // 确认按钮提交后的加载状态
      modalContent: '',
      currentDelAccount:''
    }
  }
  showModal =() => {
    
     <Modal
          visible={visible}
          title="提示"
          onOk={this.handleOk}
          onCancel={this.handleCancelModal}
          footer={[
            <Button key="submit" type="primary" size="large" loading={loading} onClick={this.handleOk}>
              确认
            </Button>,
          ]}
        >
         {modalContent}
        </Modal>
  }
  // 删除账号提示
  handleDeleteAccount = (data,e) => {
    console.log(data);
    this.setState({visible: true, modalContent: '您是否确认删除当前账号'});
  }
  // 修改账号
  handleModifyAccount = (data,e) => {
    this.setState({visible: true, modalContent: '您是否确认删除当前账号'});
  }
  // 关闭账号
  handleCloseAccount = (data,e) => {
    this.setState({
      visible: true, 
      currentDelAccount: data.accountId,
      modalContent: `您是否确认要关闭尾号<span>${data.accountId}</span>的收款账号`
  })
}
  // 添加账号
  handleAddCount = (data) => {
    // console.log(routerRedux); debugger 添加收款账户，跳转路由
    this.props.dispatch(routerRedux.push({pathname: '/addCount'}))
    // 应该在提交验证后进入下一步，APIOK的情况下，上下两个调整位置 this.props.dispatch({type:
    // 'collectionAccountManagement/submitAccountInfo', payload: data})
  }
  // 关闭弹窗
  handleCancelModal = () => {
    this.setState({visible: false})
  }
  // 
  handleOk =() => {
     this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  }
  render() {
    const {dispatch, location, accountLists} = this.props;
    let {dataSource, visible, modalContent, loading} = this.state;
    return (
      <div>
        <div className={styles.pannelTitle}>
          收款账户管理
          <span onClick={this.handleAddCount}><img src={config.addIcon}/></span>
        </div>
        <div className={styles.boxShadowBottom}>
          <h3 className={styles.tableTitle}>Amazon</h3>
          <Table
            size="small"
            pagination={{
            defaultPageSize: 3
          }}
            showHeader={true}
            bordered={false}
            columns={this.columns}
            className={styles.easywebTable}
            rowKey={record => record.accountId}
            dataSource={dataSource}></Table>
        </div>
        <div className={styles.boxShadowTop}>
          <h3 className={styles.tableTitle}>Ebay</h3>
          <Table
            size="small"
            pagination={{
            defaultPageSize: 3
          }}
            showHeader={true}
            bordered={false}
            columns={this.columns}
            className={styles.easywebTable}
            rowKey={record => record.accountId}
            dataSource={dataSource}></Table>
        </div>
          <Modal
          visible={visible}
          title="提示"
          onOk={this.handleOk}
          onCancel={this.handleCancelModal}
          footer={[
            <Button key="submit" type="primary" size="large" loading={loading} onClick={this.handleOk}>
              确认
            </Button>,
          ]}
        >
         {modalContent}
        </Modal>
      </div>
    )
  }
}

// const mapStateToProps = (state, ownProps) => {   return {     token :
// state.token,     accountLists: state.accountLists   } }

CollectionAccountManagement.propTypes = {
  dispatch: PropTypes.func,
  location: PropTypes.object
}

export default connect(({dispatch, location, accountLists}) => ({dispatch, location, accountLists}))(CollectionAccountManagement)
