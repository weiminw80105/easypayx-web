import React from 'react'
import { Card, Col, Dropdown, Icon, Menu, Row, Tag, Table, Avatar } from 'antd';

const UserLegalEntityList = ({ loading, entities }) => {

  console.log(entities)
  const content=[];
  // entities.forEach((item,index)=>{
  //   let ss = item.legalEntity.identityFiles.split(",");
  //   const _legalPicts = [];
  //   ss.forEach()
  //   content.push(
  //

  //   )
  // })
  return (
    <div>

      {

        entities.map(item => <div>
          <Row style={{borderBottom:'1px solid #ececec',paddingBottom:'16px', paddingTop:'16px'}}>
            <Col span={8}>
              <dl className="">
                <dt><strong>实名</strong></dt>
                <dd>{item.legalEntity.entityName}</dd>
                <dt><strong>证件号码</strong></dt>
                <dd>{item.legalEntity.identityNumber}</dd>
              </dl>
              <Tag color="green">实名验证通过</Tag>
            </Col>
            <Col span={16}>
              { item.legalEntity.identityFiles && item.legalEntity.identityFiles.split(',').map( _item => (
                <Row type="flex" justify="start">
                  <Col span={12}><Card bodyStyle={{ padding: 0 }}>
                    <div className="custom-image">
                      <img alt="example" width="100%" src={_item} />
                    </div>

                  </Card></Col>

                </Row>
              ))}
            </Col>
          </Row>

        </div>)
      }
    </div>
  )

}

export default UserLegalEntityList
