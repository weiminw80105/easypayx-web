import React,{Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Button, Row, Form, Input,Tabs } from 'antd'
import { config } from 'utils'
import {routerRedux} from 'dva/router'
import {AmericaSiteForm, EuropeSiteForm, EbayForm, ShareholdersForm} from './components/'
import styles from './index.less'

const TabPane = Tabs.TabPane
const FormItem = Form.Item

const AddCount = class AddCount extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentKey: "1"
    }
    this.handleTabClick = this.handleTabClick.bind(this)
    this.handleOnOk = this.handleOnOk.bind(this)
    this.handleOnCancel = this.handleOnCancel.bind(this)
  }
 
  handleTabClick = (key) => {
    this.setState({
      currentKey: key,
      isNext: false
    })
  }
  handleOnOk = (data) => {
    console.log(data);
    let {isEurope} = data;
    this.setState({
      isNext: !!isEurope
    });
    // 应该在提交验证后进入下一步，APIOK的情况下，上下两个调整位置
    this.props.dispatch({type: 'AddCount/submitAccountInfo', payload: data})
  }
  handleOnCancel =(data) => {
    let {isEurope,currentKey} = data != undefined ? data : {isEurope: false};
  //  if(isEurope != undefined){

  //  }
      this.setState({
        isNext: !!isEurope,
        currentKey: currentKey != undefined ? currentKey : this.state.currentKey
      });
    //比如返回按钮处理，应该返回哪里
    // routerRedux.push('/')
  }

  render() {
    return (
      <div className={styles.form}>
        <div className={styles.pannelTitle}>
        申请收款账户
        </div>
        {/* 企业title */}
        {
          
          this.state.currentKey == 2 && !this.state.isNext &&
          <div className={styles.enterprise}><span className={styles.enterpriseTitle}>企业信息 </span><div className={styles.spectialTips}>注：首次注册亚马逊欧洲站收款账号，请用英文完善以下信息</div></div>
        }
        {/* 欧洲站下一步表单信息 */}
        {
          (!!this.state.isNext&&this.state.currentKey == 2) && 
          <div>
            <div className={styles.enterprise}><span className={styles.enterpriseTitle}>股东信息 </span><div className={styles.spectialTips}>注：请用英文完善填写公司占股25%以上的gu懂信息</div></div>
             <ShareholdersForm 
                item={{date:'2017-09-28'}}
                handleOnOk={this.handleOnOk} 
                uptoken={this.props.token ? this.props.token.uptoken : ''}
                handleOnCancel={this.handleOnCancel}></ShareholdersForm>
          </div>
        }
        {/* 正常表单信息 */}
        {
          !this.state.isNext &&
          <div className={styles.accountForm}>
            <Tabs type="card"
              activeKey={this.state.currentKey ? this.state.currentKey+'' : '1'}
              className={styles.widthAuto}
              onTabClick={this.handleTabClick}>
              <TabPane tab="Amazon美国站" key="1">
                <AmericaSiteForm handleOnOk={this.handleOnOk} handleOnCancel={this.handleOnCancel}></AmericaSiteForm>
              </TabPane>
              <TabPane tab="Amazon欧洲站" key="2">
                <EuropeSiteForm 
                  item={{date:'2017-09-28'}}
                  handleOnOk={this.handleOnOk} 
                  handleOnCancel={this.handleOnCancel}></EuropeSiteForm>
              </TabPane>
              <TabPane tab="Ebay" key="3">
                <EbayForm></EbayForm>
              </TabPane>
            </Tabs>
          </div>
         }
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    token : state.token
  }
}

AddCount.propTypes = {
  dispatch: PropTypes.func,
  location: PropTypes.object,
}

export default connect(mapStateToProps)(AddCount)


