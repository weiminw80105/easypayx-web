import React from 'react'
import PropTypes from 'prop-types'
import {Row, Button } from 'antd'
import styles from '../index.less'
import {
    Form,
    Input,
    InputNumber,
    Radio,
    Cascader
} from 'antd'

const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
}

const AmericaSiteForm = ({
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue
    },
    handleOnOk,
    handleOnCancel
}) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
            }
            var fmData = Object.assign({},data,{
                url: '/americasitesubmit'
            })
            handleOnOk(data);
        })
    }
    const handleCancel = () => {
        handleOnCancel({
            isEurope: false
        })
    }

    return (
        <div className={styles.americaSite}>
            <Form layout="horizontal">
                <FormItem label="店铺名称" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('name', {
                        initialValue: "",
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input/>)}
                </FormItem>
                <FormItem label="Seller ID" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('sellerId', {
                        initialValue: "",
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input/>)}
                </FormItem>
                <FormItem label="AWS Access Key" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('accessKey', {
                        initialValue: "",
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input/>)}
                </FormItem>
                <FormItem label="Secret Key" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('secretKey', {
                        initialValue: "",
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input/>)}
                    <a href="" className={styles.getTipAnchor}>如何获得以上信息？</a>
                </FormItem>
                <Row className={styles.commonRow}>
                    <Button type="primary" size="large" className={styles.btnBgColor} onClick={handleOk}>提交</Button><span className={styles.returnAnchor} onClick={handleCancel}>返回</span>
                </Row>
            </Form>
        </div>
    )
}

AmericaSiteForm.propTypes = {
    form: PropTypes.object.isRequired,
    handleOnOk: PropTypes.func.isRequired,
    handleOnCancel: PropTypes.func.isRequired,
}

export default Form.create()(AmericaSiteForm)
