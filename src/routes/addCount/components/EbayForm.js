import React from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Cascader} from 'antd'

const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
}

const EbayForm = ({
    item = {},
    onOk,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue
    }
}) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
                key: item.key
            }
            data.address = data
                .address
                .join(' ')
            onOk(data)
        })
    }

    return (
        <div >
            <Form layout="horizontal">
                <FormItem label="Name" hasFeedback {...formItemLayout}>
                    {getFieldDecorator('name', {
                        initialValue: item.name,
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input/>)}
                </FormItem>
            </Form>
        </div>
    )
}

EbayForm.propTypes = {
    form: PropTypes.object.isRequired,
    type: PropTypes.string,
    item: PropTypes.object,
    onOk: PropTypes.func
}

export default Form.create()(EbayForm)
