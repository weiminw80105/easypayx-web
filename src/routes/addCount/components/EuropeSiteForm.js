import React from 'react'
import PropTypes from 'prop-types'
import {Form, Row, Col, Input, InputNumber,DatePicker, Button} from 'antd'
import moment from 'moment'
import 'moment/locale/zh-cn'
import styles from '../index.less'

moment.locale('zh-cn');

const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
}

const EuropeSiteForm = ({
    item = {},
    handleOnOk,
    handleOnCancel,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue
    }
}) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
                key: item.key
            }
            data.isEurope = true;
            handleOnOk(data)
        })
    }

    const datePickerStyle = {
        "width": "100%"
    }
    return (
        <div >
            <Form layout="horizontal">
                <div className={styles.boxShadowBottom}>
                    <Row>
                        <Col span={12}>
                            <FormItem label="企业英文名" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('name', {
                                    initialValue: '',
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="所属行业" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('belong', {
                                    initialValue: '',
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="营业执照号" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('businessLicense', {
                                    initialValue: '',
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="注册地址" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('registerAddress', {
                                    initialValue: '',
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="颁发日期" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('publishDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="有效期至" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('expireDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="企业类型" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('exterpriseType', {
                                    initialValue: item.exterpriseType,
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="股东人数" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('shareholders', {
                                    initialValue: item.shareholders,
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                                <span className={styles.subTips}>注：仅需填写占股25%以上股东数量，如全部低于25%，请填写0</span>
                            </FormItem>
                        </Col>
                    </Row>
                </div>
                <div className={styles.boxShadowTop}>
                        <div className={styles.artificialTitle}>法人信息</div>
                        <Row>
                        <Col span={12}>
                            <FormItem label="姓名" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('username', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="生日" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('birthday', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="居住地址" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('residenceAddress', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="身份证号" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('identifyCard', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="签发日期" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('issueDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="有效期至" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('artiExpireDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="签发机构" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('issueInstitution', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                </div>
                 <Row className={styles.commonRow}>
                    <Button type="primary" size="large" 
                    className={styles.btnBgColor} onClick={handleOk}>下一步</Button><span className={styles.returnAnchor} onClick={handleOnCancel}>返回</span>
                </Row>
            </Form>
        </div>
    )
}

EuropeSiteForm.propTypes = {
    form: PropTypes.object.isRequired,
    type: PropTypes.string,
    item: PropTypes.object,
    handleOnOk: PropTypes.func.isRequired,
    handleOnCancel: PropTypes.func.isRequired,
}

export default Form.create()(EuropeSiteForm)
