import AmericaSiteForm from './AmericaSiteForm'
import EbayForm from './EbayForm'
import EuropeSiteForm from './EuropeSiteForm'
import ShareholdersForm from './ShareholdersForm'

export default  {
    AmericaSiteForm,
    EuropeSiteForm,
    EbayForm,
    ShareholdersForm
}