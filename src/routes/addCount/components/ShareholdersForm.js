import React from 'react'
import PropTypes from 'prop-types'
import {
    Form,
    Row,
    Col,
    Input,
    InputNumber,
    DatePicker,
    Button
} from 'antd'
import SinglePictureUpload from "../../../components/SinglePictureUpload/SinglePictureUpload.js";
import moment from 'moment'
import 'moment/locale/zh-cn'
import styles from '../index.less'

moment.locale('zh-cn');

const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6
    },
    wrapperCol: {
        span: 14
    }
}

const ShareholdersForm = ({
    item = {},
    handleOnOk,
    handleOnCancel,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue
    },
uptoken
}) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
                key: item.key
            }
            handleOnOk(data)
        })
    }
    const handleCancel = () => {
        handleOnCancel({
            isEurope: false,
            currentKey: 2
        })
    }
const handleChangeFileUpload = (value) => {
    console.log(value);
}
    const datePickerStyle = {
        "width": "100%"
    }
    return (
        <div className={styles.shareholdersForm}>
            <Form layout="horizontal">
                <div>
                    <Row>
                        <Col span={12}>
                            <FormItem label="姓名" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('username', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="生日" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('birthday', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="居住地址" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('residenceAddress', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="身份证号" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('identifyCard', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="签发日期" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('issueDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="有效期至" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('artiExpireDate', {
                                    initialValue: moment('2015/01/01'),
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<DatePicker style={datePickerStyle} format="YYYY/MM/DD"/>)}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <FormItem label="签发机构" hasFeedback {...formItemLayout}>
                                {getFieldDecorator('issueInstitution', {
                                    initialValue: "",
                                    rules: [
                                        {
                                            required: true
                                        }
                                    ]
                                })(<Input/>)}
                            </FormItem>
                        </Col>
                    </Row>
                </div>
                 <div className={styles.boxShadowTop}>
                    <div className={styles.artificialTitle}>证件上传</div>
                    <FormItem
                        className={styles.authFormItem}
                        label="企业营业执照"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                    >
                        {getFieldDecorator('identityFiles_1', {
                        rules: [{ required: true, message: '请上传企业营业执照!' }],
                        })(
                        <SinglePictureUpload
                            uptoken={uptoken}
                            onChange={handleChangeFileUpload}></SinglePictureUpload>
                        )}
                    </FormItem>
                </div>
                <Row className={styles.commonRow}>
                    <Button
                        type="primary"
                        size="large"
                        className={styles.btnBgColor}
                        onClick={handleOk}>提交</Button>
                    <span className={styles.returnAnchor} onClick={handleCancel}>返回</span>
                </Row>
            </Form>
        </div>
    )
}

ShareholdersForm.propTypes = {
    form: PropTypes.object.isRequired,
    type: PropTypes.string,
    item: PropTypes.object,
    handleOnOk: PropTypes.func.isRequired,
    handleOnCancel: PropTypes.func.isRequired,
    uptoken: PropTypes.string.isRequired,
}

export default Form.create()(ShareholdersForm)
