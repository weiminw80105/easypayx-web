import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Button, Row, Col, Table, Card } from 'antd'
import { config } from 'utils'
import styles from './index.less'
import QueryString from 'query-string'
import {UserTransactions,UserView} from './components/'



const Wallet = ({ loading, dispatch, wallet,app}) => {


  return (
    <div className={styles.form}>
      <div className={styles.pannelTitle}>
        <div>
          <p>我的钱包</p>
        </div>
        <div className={styles.link1}>
          <Button type='ghost' size='default' color='red'>中国站</Button>
        </div>
        <div className={styles.link2}>
          <Button type='ghost' size='default' color='red'>国际站</Button>
        </div>
      </div>
      <Row gutter={0}>

        <Col lg={12} md={24}>
          <UserView data={app.user} color="blue" icon="pay-circle" title="RMB"/>
        </Col>

        <Col lg={12} md={24}>
          <UserView data={app.user} color="green" icon="pay-circle" title="USD"/>
        </Col>


      </Row>
      <Row gutter={0}>
        <Col lg={12} md={24}>
          <UserView data={app.user} color="blue" icon="pay-circle" title="EUR"/>
        </Col>

        <Col lg={12} md={24}>
          <UserView data={app.user} color="green" icon="pay-circle" title="JPY"/>
        </Col>
      </Row>

      <Row gutter={0}>
        <Col lg={24} md={24}>

            <UserTransactions data={wallet.userTransactions}/>

        </Col>

      </Row>
    </div>
  )
}

Wallet.propTypes = {
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  wallet: PropTypes.object,
}

export default connect(({ loading, wallet, app }) => ({ loading, wallet, app }))(Wallet)
