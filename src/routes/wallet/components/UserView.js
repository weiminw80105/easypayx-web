import React, { PropTypes } from 'react'
import { Icon, Card } from 'antd'
import styles from '../index.less'
import CountUp from 'react-countup'

const countUpProps = {
  start: 0,
  duration: 2.75,
  useEasing: true,
  useGrouping: true,
  separator: ','
}


function UserView ({ icon, color, title, data, countUp }) {
  return (
    <Card className={styles.numberCard} bordered={false} bodyStyle={{padding: 0}}>
      <Icon className={styles.iconWarp} style={{ color }} type={icon} />
      <div className={styles.content}>

        <div className={styles.title}>
          <p>{title || 'No Title'}</p>
          <p style={{color: color}}><CountUp
            end={data.balance}
            {...countUpProps}
          /></p>
        </div>



        <p>
          <Icon type="red-envelope" className={styles.icon}/>
          <Icon type="bank" className={styles.icon}/>
          <Icon type="wallet" className={styles.icon}/>
        </p>
      </div>
    </Card>
  )
}

UserView.propTypes = {
  data: PropTypes.object
}

export default UserView
