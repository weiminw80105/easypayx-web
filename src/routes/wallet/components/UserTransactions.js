import React, { PropTypes } from 'react'
import { Table, Tag, Card } from 'antd'
import styles from '../index.less'
import { color } from '../../../utils'
import { currency } from '../../../constants';


function UserTransactions ({ data }) {
  const status = {
    16: {
      color: color.green,
      text: '成功'
    },
    18: {
      color: color.yellow,
      text: '处理中'
    },
    32: {
      color: color.red,
      text: '失败'
    },
    34: {
      color: color.blue,
      text: '处理中'
    }
  }
  const columns = [
    {
      title: '日期',
      dataIndex: 'transactionTime',
      key: 'transactionTime'
    },
    {
      title: '金额',
      dataIndex: 'amount',
      key: 'amount',
      render: (text, record, index) => {
        return (
          <div><label>$</label> {text}</div>
        )
      }
    },
    {
      title: '交易类型',
      dataIndex: 'accountNumber',
      key: 'accountNumber',
      render: (text, record, index) => {
        var transactionType;
        if (record.transactionType === 1) {
          transactionType = '入账';
        } else if (record.transactionType === 2) {
          transactionType = '提现';
        }
        return (
          <div>
            <div><strong>{transactionType}</strong></div>
          </div>
        )
      }
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => <Tag color={status[text].color}>{status[text].text}</Tag>

    },
    {
      title: '',
      dataIndex: 'operation',
      key: 'operation',
      render: (text, record, index) => {
        return (
          <div><a href>详情</a></div>
        )
      }
    },
  ];
  return (
    <Card title="最近一周" extra={<div><a href="#">查看全部账单</a></div>} className={styles.transaction}>

      <div>
        <Table
          size="small"
          columns={columns}
          dataSource={data}
          rowKey={record => record.transactionKey}
          pagination={true}
        />
      </div>
    </Card>
  )
}

UserTransactions.propTypes = {
  data: PropTypes.array
}

export default UserTransactions

