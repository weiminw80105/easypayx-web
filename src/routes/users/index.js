import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Card, Col, Dropdown, Icon, Menu, Row, Tag, Table, Avatar } from 'antd';
import { config } from 'utils';
import styles from './index.less';
import { DataTable } from 'components';

const Users = ({ loading, dispatch, users }) => {

  console.log(users)
  // const { users } = datas;
  const columns = [
    { title: '#', dataIndex: 'id', key: 'id' },
    { title: 'UID', dataIndex: 'userId', key: 'userId' },
    { title: 'ID', dataIndex: 'identityNumber', key: 'identityNumber' },
    { title: 'EN', dataIndex: 'entityName', key: 'status' },
    {
      title: '', render: (record) => <Dropdown overlay={menu} trigger={['click']}>
      <a className="ant-dropdown-link" href="#">
        <Icon type="ellipsis"></Icon> <Icon type="down"/>
      </a>
    </Dropdown>
    },
  ]
  const columns2 = [{
    title:"放款时间",
    dataIndex:"fundDateEnd"
  }, {
    title:"金额",
    dataIndex:"amount"
  },{
    title:"金额",
    dataIndex:"currency"
  }];
  const dataSource = [{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'USD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },{
    fundDateEnd:'2017-09-10 11:11:00',
    amount:'1245.12',
    currency:'CAD'
  },]

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a href="http://www.alipay.com/">实名通过</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="1">
        <a href="http://www.taobao.com/">证件照片不清楚</a>
      </Menu.Item>
      <Menu.Item key="3">无法查询实名信息</Menu.Item>
    </Menu>
  )
  return (
    <div>
      {/*<Row gutter={32}>*/}
      {/*<Col span={12}>*/}
      {/*<Card bordered={false} >*/}
      {/*<DataTable*/}
      {/*columns={columns}*/}
      {/*dataSource={users.auths}*/}
      {/*/>*/}
      {/*</Card>*/}

      {/*</Col>*/}
      {/*</Row>*/}

      {/*<Card bordered={false} className={styles.card}>
       <Row gutter={64}>
       <Col span={8} className="text-right"><label>实名信息</label></Col>
       <Col span={16} className="text-left">fdfddf</Col>
       </Row>
       </Card>*/}
      <Row gutter={32}>

        <Col span={12}>
          {/*<div style={{float:'left'}}>*/}
            {/*<Avatar size="large" icon="user" shape="circle"/>*/}

          {/*</div>*/}
          {/*<div style={{float:'left',marginLeft:'4px'}}>*/}
            {/*<div style={{fontSize:'14px'}}>william@fdfdfdfdfd.com</div>*/}
            {/*<Table size="small" pagination={{defaultPageSize:3}} showHeader={true} bordered={false} columns={columns2} dataSource={dataSource}></Table>*/}

          {/*</div>*/}
          <Card>
            {/*<Row type="flex" justify="center" align="middle">*/}
              {/*<Col span={24} className="text-center">*/}
                {/*<Avatar icon="user" size="large" />*/}
                {/*<div style={{ marginTop: '16px' }}>943065748@qq.com</div>*/}
              {/*</Col>*/}
            {/*</Row>*/}
            <Row>
              <Col>
                <div style={{float:'left'}}>
                  <Avatar size="default" icon="user" shape="circle"/>

                </div>
                <div style={{float:'left',marginLeft:'8px'}}>
                  <div className={styles.text}>#164</div>
                  <div className={styles.text}>943065748@qq.com</div>
                </div>
                <div style={{float:'right',marginLeft:'8px'}}>
                  <span style={{fontSize:'28px'}}>$122442.00</span>
                </div>
              </Col>

            </Row>

            <Row>
              <Col>
                <hr style={{marginTop:'8px'}} />
              </Col>
            </Row>



            {/*<Row>*/}
              {/*<Col span={12}>*/}
                {/*<div className="text-center"><Avatar size="large" icon="user" shape="circle"/></div>*/}
                {/*<div className="text-center">#164</div>*/}
                {/*<div className="text-center"><label>EMAIL:</label>  943065748@qq.com</div>*/}
              {/*</Col>*/}
            {/*</Row>*/}

          </Card>

        </Col>

        <Col sm={24} xs={24} md={12} lg={12} xl={8}>
          <Row>
            <Col>
              <Card className={styles.card} title="实名信息">
                <Row>
                  <Col span={8}>
                    <dl className="">
                      <dt><strong>实名</strong></dt>
                      <dd>邓育铭</dd>
                      <dt><strong>证件号码</strong></dt>
                      <dd>440902199512280136</dd>
                    </dl>
                    <Tag color="green">实名验证通过</Tag>
                  </Col>
                  <Col span={16}>
                    <Row type="flex" justify="start">
                      <Col span={12}><Card bodyStyle={{ padding: 0 }}>
                        <div className="custom-image">
                          <img alt="example" width="100%" src="https://id.transfer-express.net/Fq-ldc0Axb2kGUpeh6RupTxFB7NV"/>
                        </div>

                      </Card></Col>
                      <Col span={12}><Card bodyStyle={{ padding: 0 }}>
                        <div className="custom-image">
                          <img alt="example" width="100%" src="https://id.transfer-express.net/Fpx2C7jjt3VxnupXc2aNSACS58uS"/>
                        </div>
                      </Card></Col>
                    </Row>
                  </Col>
                </Row>
              </Card>
            </Col>


            <Col style={{ marginTop: '16px' }}>
              <Card className={styles.card} title="亚马逊店铺" bordered={false}>
                <Row style={{borderBottom:"1px solid #ececec"}}>
                  <Col span={8}>
                    <div style={{fontSize:'18px'}}>Dddu Story</div>

                    <Tag color="green">实名验证通过</Tag>
                    <div style={{marginTop:'16px'}}>
                      <dl className="">
                        <dt><strong>收款银行账号</strong></dt>
                        <dd>9108646760</dd>
                        <dt><strong>收款银行路由号</strong></dt>
                        <dd>251022120</dd>
                      </dl>
                    </div>
                  </Col>
                  <Col span={16}>
                    <Table size="small" pagination={{defaultPageSize:3}} showHeader={true} bordered={false} columns={columns2} dataSource={dataSource}></Table>
                  </Col>
                </Row>
                <Row style={{marginTop:'16px',borderBottom:"1px solid #ececec"}}>
                  <Col span={8}>
                    <div style={{fontSize:'18px'}}>Dddu Story</div>

                    <Tag color="green">实名验证通过</Tag>
                    <div style={{marginTop:'16px'}}>
                      <dl className="dl-horizontal">
                        <dt><strong>收款银行账号</strong></dt>
                        <dd>9108646760</dd>
                        <dt><strong>收款银行路由号</strong></dt>
                        <dd>251022120</dd>
                      </dl>
                    </div>
                  </Col>
                  <Col span={16}>
                    <Table size="small" pagination={{defaultPageSize:3}} showHeader={true} bordered={false} columns={columns2} dataSource={dataSource}></Table>
                  </Col>
                </Row>
              </Card>
            </Col>

            <Col style={{ marginTop: '16px' }}>
              <Card className={styles.card} title="EBAY店铺">
                <Row>
                  <Col span={8}>
                    <dl className="">
                      <dt><strong>实名</strong></dt>
                      <dd>邓育铭</dd>
                      <dt><strong>证件号码</strong></dt>
                      <dd>440902199512280136</dd>
                    </dl>
                    <Tag color="green">实名验证通过</Tag>
                  </Col>
                  <Col span={16}>
                    <Row type="flex" justify="start">
                      <Col span={12}><Card bodyStyle={{ padding: 0 }}>
                        <div className="custom-image">
                          <img alt="example" width="100%" src="https://id.transfer-express.net/Fq-ldc0Axb2kGUpeh6RupTxFB7NV"/>
                        </div>

                      </Card></Col>
                      <Col span={12}><Card bodyStyle={{ padding: 0 }}>
                        <div className="custom-image">
                          <img alt="example" width="100%" src="https://id.transfer-express.net/Fpx2C7jjt3VxnupXc2aNSACS58uS"/>
                        </div>
                      </Card></Col>
                    </Row>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>

        </Col>



      </Row>
    </div>
  )
}

Users.propTypes = {
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  dashboard: PropTypes.object,
}

export default connect(({ loading, users }) => ({ loading, users }))(Users)
