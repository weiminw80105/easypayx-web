import * as bankService from '../services/banks';
import queryString from 'query-string'

export default {
  namespace: 'withdraw',
  state: {
    userBanks: [],
    withdrawTransaction: null,
  },
  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/withdraw') {
          dispatch({
            type: 'fetchUserBanks',
            payload: queryString.parse(location.search),
          })
          dispatch({ type: 'updateState', payload: { withdrawTransaction: null} });
        }
      })
    },
  },
  effects: {
    *fetchUserBanks({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(bankService.fetchUserBanks, { userId });
      yield put({ type: 'fetchUserBanksSuccess', payload: { userBanks:data.list } });
    },
    *submitWithdrawRequest(
      { onError, payload:
        { userId, userDigitalAccountId, bankAccount, amount, currency, transPasswd } }
      , { call, put }){
      try{
        const { data } = yield call(bankService.submitWithdraw, {
          userId,
          userDigitalAccountId,
          bankAccount,
          amount,
          currency,
          transPasswd,
        });
        yield put({ type: 'saveWithdrawResponse', payload: { data } });
      } catch (e) {
        onError(e);
      }

    }
  },

  reducers: {
    fetchUserBanksSuccess (state, { payload }) {
      return { ...state, ...payload }
    },
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
    saveWithdrawResponse(state, { payload: { data: withdrawTransaction,error } }) {
      console.log("{ ...state, withdrawTransaction }");
      console.log({ ...state, withdrawTransaction });
      return { ...state, withdrawTransaction };
    },
  },
};
