/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import { create, remove, update } from 'services/user'
import { fetchUserAuths } from 'services/authentications'
import * as usersService from 'services/users'
import queryString from 'query-string'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'users',

  state: {
    users: [],
    auths: [],
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/users') {
          dispatch({
            type: 'fetchUserAuths',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * fetchUserAuths ({ payload },{ call, put}) {
      const users = [{
        id: 1,
        userName: "11",
        status: 1,
        email: "fdfd",


      }, {
        id: 2,
        userName: "11",
        status: 1,
        email: "fdfd",


      }]
      const { data, header } = yield call(fetchUserAuths);
      console.log(data)
      yield put({ type: 'fetchUsersSuccess', payload: { auths:data.list } })
    },

  },

  reducers: {
    fetchUsersSuccess (state, { payload }) {
      // console.log(payload)
      // console.log(Object.assign({}, state, payload))
      return Object.assign({}, state, payload)
    },
  },
})
