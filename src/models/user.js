/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import { queryUserLegalEntities } from '../services/user'
import { fetchUserAuths } from 'services/authentications'
import * as usersService from 'services/users'
import queryString from 'query-string'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'user',

  state: {
    userLegalEntities: [],
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        console.log(location)
        alert(2)
        if (location.pathname === '/user/301') {
          
          dispatch({
            type: 'fetchUserLegalEntites',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * fetchUserDetail ({ payload },{ call, put}) {
      const users = [{
        id: 1,
        userName: "11",
        status: 1,
        email: "fdfd",


      }, {
        id: 2,
        userName: "11",
        status: 1,
        email: "fdfd",


      }]
      const { data, header } = yield call(fetchUserAuths);
      console.log(data)
      yield put({ type: 'save', payload: { auths:data.list } })
    },

    * fetchUserLegalEntites({ payload }, {call, put}) {
      console.log("----")
      const { data, header } = yield call(queryUserLegalEntities, { userId: 301 });
      console.log(data);
      yield put({ type: 'save', payload: { userLegalEntities:data.list } })
    }
  },

  reducers: {
    save (state, { payload }) {
      // console.log(payload)
      // console.log(Object.assign({}, state, payload))
      return Object.assign({}, state, payload)
    },
  },
})
