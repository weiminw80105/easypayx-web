/* global window */
import modelExtend from 'dva-model-extend'
import { fetchGatewayUserInfo, fetchUserTransactions } from 'services/users'
import queryString from 'query-string'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'wallet',

  state: {
    usersAccount: 0,
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/wallet') {
          dispatch({
            type: 'fetchUserTransactions',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * fetchUserTransactions ({ payload },{ call, put}) {
      const { data, headers } = yield call(fetchUserTransactions, payload)
      //let userTransactions = data.list;
      yield put({ type: 'fetchUserTransactionsSuccess', payload: { userTransactions:data.list } })
      //yield put({ type: 'fetchUserTransactionsSuccess', payload: {userTransactions} })
    },
  },

  reducers: {
    fetchUserTransactionsSuccess (state, { payload }) {
      return { ...state, ...payload }
    },
  },



})
