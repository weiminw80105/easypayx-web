/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import { create, remove, update } from 'services/user'
import * as usersService from 'services/users'
import queryString from 'query-string'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'dashboard',

  state: {
    users: [],
    usersAccount: 0,
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/dashboard') {
          dispatch({
            type: 'fetchUsers',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * fetchUsers ({ payload },{ call, put}) {
      const users = [{
        id: 1,
        userName: "11",
        status: 1,
        email: "fdfd",


      }]
      yield put({ type: 'fetchUsersSuccess', payload: {users} })
    },

  },

  reducers: {
    fetchUsersSuccess (state, { payload }) {
      return { ...state, ...payload }
    },
  },
})
