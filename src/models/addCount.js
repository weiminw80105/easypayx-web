import {routerRedux} from 'dva/router'
import {submitAuthtication, fetchToken, getAccoutUsers} from 'services/collectionAccountManagement'

export default {
  namespace : 'addCount',
  state : {
    isSubmitSuccess: false
  },

  effects : {
    *fetchUploadToken({}, {call, put}) {
      const {data} = yield call(fetchToken, {});
      yield put({type: 'save', payload: {
          data
        }});
    },
    * submitAccountInfo({
      payload
    }, {put, call, select}) {
      const data = yield call(submitAuthtication, payload)
      if (data.url) {
        // yield put({ type: 'app/query' })

      } else {
        throw data
      }
    },
  },
  reducer : {
    save(state, {
      payload: {
        data: token
      }
    }) {
      return {
        ...state,
        token
      };
    },
    
  },
subscriptions:{
  setup({dispatch, history}) {
    return history.listen(({pathname, query}) => {
      console.log(pathname);
      if (pathname == '/addCount') {
        dispatch({type: 'fetchUploadToken'});
      }
    });
  }
}
}
