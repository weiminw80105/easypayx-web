import {routerRedux} from 'dva/router'
import {submitAuthtication, fetchToken, getAccoutUsers, deleteAccoutUsers} from 'services/collectionAccountManagement'
import queryString from 'query-string'

export default {
  namespace : 'collectionAccountManagement',
  state : {
    isSubmitSuccess: false,
    accoutLists: []
  },

  effects : {
    *fetchUploadToken({},{ call, put }) {
      const { data } = yield call(fetchToken, { });
      yield put({ type: 'save', payload: { data } });
    },
    *submitAccountInfo({
      payload
    }, {put, call, select}) {
      const data = yield call(submitAuthtication, payload)
      if (data.url) {
        // yield put({ type: 'app/query' })
        
      } else {
        throw data
      }
    },
    *getAccoutUsers({
        payload
      }, {put, call, select}) {
        console.log(yield call(getAccoutUsers, payload));
        const { data } = yield call(getAccoutUsers, payload);
        yield put({ type: 'updateAccoutUser', payload: { accoutLists:data } })
    },
    *deleteAccount({
      payload
    },{put, call,select}){
      const { res } = yield call(deleteAccoutUsers, payload);
      const {data} = yield call(getAccoutUsers, payload);
      yield put({ type: 'updateAccoutUser', payload: { accoutLists:data } })
    }
  },
  reducer: {
    save(state, { payload: { data: token } }) {
      return { ...state, token };
    },
    updateAccoutUser(state, {payload}){
       return { ...state, payload };
    }
  },
  subscriptions:{
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/collectionAccountManagement') {
          dispatch({
            type: 'getAccoutUsers',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  }
}
