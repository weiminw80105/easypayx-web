import React, { PropTypes } from 'react'
import { Icon, Card } from 'antd'
import styles from './UserView.less'
import CountUp from 'react-countup'
import { color } from '../../utils'


const countUpProps = {
  start: 0,
  duration: 2.75,
  useEasing: true,
  useGrouping: true,
  separator: ','
}


function UserView ({ icon, color, title, data, countUp }) {
  let avatar='../../img/人民币.png'
  return (
    <Card className={styles.numberCard} bordered={false} bodyStyle={{padding: 0}}>
      <Icon className={styles.iconWarp} style={{ color }} type={icon} />
      <div className={styles.content}>
        <p className={styles.title}>{title || 'No Title'}

        </p>

        <p className={styles.number}>
          <CountUp
            end={data.balance}
            {...countUpProps}
          />
        </p>
      </div>
    </Card>
  )
}

UserView.propTypes = {
  data: PropTypes.object
}

export default UserView
