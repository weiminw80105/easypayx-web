import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Switch } from 'antd'
import { config } from 'utils'
import styles from './Layout.less'
import Menus from './Menu'

const Sider = ({ siderFold, darkTheme, location, navOpenKeys, changeOpenKeys, menu, user }) => {
  const menusProps = {
    menu,
    siderFold,
    darkTheme,
    location,
    navOpenKeys,
    changeOpenKeys,
  }
  return (

    <div>
      <img className={styles.logo} src={config.logo} />
      <div className={styles.logo}>
        {siderFold ? '' : <span>账户名：<span className={styles.username}>{user.username}</span></span>}
      </div>
      <div className={styles.logo}>
        {siderFold ? '' : <span>账户号：<span className={styles.useremail}>{user.email}</span></span>}
      </div>
      <div>
        <h3 className={styles.level0Title}>账户概览</h3>
        <Menus {...menusProps} />
      </div>
    </div>
  )
}

Sider.propTypes = {
  menu: PropTypes.array,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool,
  location: PropTypes.object,
  changeTheme: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
  user: PropTypes.object,
}

export default Sider
